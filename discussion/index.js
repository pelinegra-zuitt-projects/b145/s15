// let's include a confirm message
// console.log() is used to output
console.log('Hello From JS');
// There are multiple ways to display messages in the console using different formats
// error message
console.error('Error something');

// Warning message
console.warn('Ooops Warning');

// syntax:
// function nameofFunction() {
// 	//instruction/procedures
// 	return
// }

// Let's create a function that will display an error
function errorMessage(){
	console.error('Errorrr oh No!!!')
}

errorMessage(); 
// callout the function

function greetings() { //Declaration
	// procedures
	console.log('Salutations from JS!');
} 

//invocation --call out

greetings();

// There are different methods in declaring a function in JS
// [SECTION] Variables declared outside a function can be used INSIDE a function.

//Let's create a function that will utilize the information outside its scope.
function fullName(){
	//combine the values of the information outside and display it inside the console

		console.log(givenName +' '+ familyName);
}

// Invoking/calling out functions, we do it by adding parenthesis() after the function name.
// we can declare outside the declaration of the function


let givenName = 'John';
let familyName = 'Smith';

fullName();

// [SECTION 2:] "blocked scope" variables, this means that variables can only be used within the scope of the function

// lets create a function that will allow us to compute the values of 2 variables.

function computeTotal() {
	let numA = 20;
	let numB = 5;
	// add the values and display it inside the console
	console.log(numA + numB);
}

// call out the function
computeTotal()

// [SECTION 3:] Functions with Parameters
// "Parameter" acts as a variable or a container/catchers that only exists inside a function.  //A parameter is used to store info that is provided to a funciton when it is called or invoked.

//lets create a function that emulate a pokemon battle

function pokemon(pangalan) {
// we're going to use the "parameter" declared on this function to be processed and displayed inside the console.
console.log('I Choose You: ' + pangalan);
}
// invocation
pokemon('Pikachu');

// parameters vs arguments
// Argument - actual value that is provided inside a function for it to work properly. 
// The term argument is used when functions are called / invoked as compared to parameters when a function is simply declared

// [SECTION 4:] Functions with Multiple parameters
// Let's declare a function that will get the sum of multiple values 
function addNumbers(numA, numB, numC, numD, numE) {
// display the sum inside the console
	console.log(numA + numB + numC + numD + numE);
}

addNumbers(1,2,3,4,5);

// Let's create a function that will generate a person's full name
function createFullName(fName, mName, lName){
	console.log(lName + ', ' + fName + '' + mName);
}
//invoke the function and pass down the arguments needed that will take the place of each parameter.
createFullName('Juan', 'Dela', 'Cruz');

// Upon using multiple arguments, it will correspond to the number of "parameters" declared in a function in succeeding order unless reordered inside the function. 

// [SECTION 5]: Using variables as arguments

// create a function that will display the stats of a pokemon in battle
let attackA = 'Tackle';
let attackB = 'Thunderbolt';
let attackC = 'Quick Attack';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack){
	console.log(name + ' use ' + attack);
}

pokemonStats(selectedPokemon, attackC);
// Ratata use Quick Attack

// Note: using variables as arguments will allow us to utilize code reusability, this will help us reduce the amoutn of code that we have to write down

//Identify the use of the "return" expression/statement
 //The return statement is used to display the output of a function. 

 // [SECTION 6:] The Return statement
 // create a function that will return a string message.
 function returnString(){
 	return 'Hello World'
 	2 + 1;
 	return 'Print me' 
 }

 // let's repackage the result of this function inside a new variable
console.log(returnString());

// lets create a simple function to demonstrate the use and behavior of the return statement again.
function dialog() {
	console.log('Ooops! I did it Again');
	console.log("Don't you know that you're toxic");
	'Im a slave for you!'
	return console.log('Sometimes I run!')

}

// note: any Block / line of code that will come after our "return" statement is going to be ignored because it came after the end of the function execution
// The main purpuse of the return statement is to identify which will be the final output/result of the function and at the same time determine the end of the function statement. 

// invocation
console.log(dialog())

// [SECTION 7: Using functions as arguments]

// Function parameters can also accept other functions as their arguments
// Some complex functions use other functions as their main arguments to perform more complex results
// we're just going to create a simple function to be used as an argument later

function argumentSaFunction(){
	console.log('This function was passed as an argument');
}

function invokeFunction(argumentNaFunction, pangalawangFunction) {
	argumentNaFunction();
	pangalawangFunction(selectedPokemon, attackB);
}
// invoke the function above
invokeFunction(argumentSaFunction, pokemonStats);


// for review**************************

	// function argumentSaFunction() {
	//   console.log('This function was passed as an argument');
	// }

	// function invokeFunction(argumentNaFunction, pangalawangFunction) {
	//    argumentNaFunction(); 
	//    pangalawangFunction(selectedPokemon, attackB);
	//    pangalawangFunction(selectedPokemon, attackC);
	// }

	// //invoke the function above
	// invokeFunction(argumentSaFunction, pokemonStats);

	// //IF YOU WANT to see details/information about a function
	// console.log(invokeFunction); //finding more information about a function inside the console.


